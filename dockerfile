FROM php:7.1-cli

# Install linux packages
RUN apt-get update
RUN apt-get install -y zip zlib1g-dev libpq-dev libicu-dev libxml2-dev
RUN apt-get install -y libbz2-dev \
    && docker-php-ext-install bz2
RUN apt-get install -y libpng-dev

# Install PHP extensions
RUN docker-php-ext-install zip intl

# Install composer
RUN curl -sS https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer

#CONFIG locale Time/Lang
RUN apt-get install -y locales
RUN echo "America/Sao_Paulo" > /etc/timezone && \
  dpkg-reconfigure -f noninteractive tzdata && \
  sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
  sed -i -e 's/# pt_BR.UTF-8 UTF-8/pt_BR.UTF-8 UTF-8/' /etc/locale.gen && \
  echo 'LANG="pt_BR.UTF-8"'>/etc/default/locale && \
  dpkg-reconfigure --frontend=noninteractive locales && \
  update-locale LANG=pt_BR.UTF-8

ENV LC_ALL=pt_BR.UTF-8
ENV LANG=pt_BR.UTF-8
ENV LANGUAGE=pt_BR.UTF-8

RUN echo "alias phpunit='/var/www/html/vendor/bin/phpunit --colors'" >> ~/.bashrc

EXPOSE 8080

WORKDIR /var/www/html/
